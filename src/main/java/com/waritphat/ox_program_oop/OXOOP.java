/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.ox_program_oop;

/**
 *
 * @author domem
 */
public class OXOOP {

    public static void main(String[] args) {
        Game game = new Game();
        game.showWelcome();
        game.newBoard();
        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if (game.isFinish()) {
                game.showTable();
                game.showResult();
                game.showStat();
                System.out.println("=====================");
                if (game.next() == true){
                    game.newBoard();
                }else{
                    break;
                }
            }
        }
    }
}
